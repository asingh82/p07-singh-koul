//
//  WhackingClass.swift
//  GameOn
//
//  Created by Vikas Koul on 5/1/17.
//  Copyright © 2017 AnshimaSingh. All rights reserved.
//

import UIKit
import SpriteKit

class WhackingClass: SKNode {
    var charNode: SKSpriteNode!
    
    var isVisible = false
    var isHit = false
    
    func configureAtPosition(pos : CGPoint){
        position = pos
        
        //Adding the hole to the scene
        let holeSprite = SKSpriteNode(imageNamed: "hole")
        holeSprite.zPosition = 1
        holeSprite.size = CGSize(width: 60, height: 30)
        addChild(holeSprite)
        
        //Cropping the zombie character
        let cropNode = SKCropNode()
        cropNode.position = CGPoint(x: 0, y: 15)
        cropNode.zPosition = 1
        cropNode.maskNode = SKSpriteNode(imageNamed: "whackMask")
        
        //Adding zombie character to the game
        charNode = SKSpriteNode(imageNamed: "zombie")
        charNode.position = CGPoint(x: 0, y: -75)
        charNode.size = CGSize(width: 50, height: 75)
        charNode.name = "zombieCharacter"
        cropNode.addChild(charNode)
        addChild(cropNode)
    }
    
    //Function to pop the zombie from the hole
    func show(hideTime: Double){
        if isVisible{
            return
        }
        charNode.run(SKAction.moveBy(x: 0, y: 85, duration: 0.005))
        isVisible = true
        isHit = false
        charNode.texture = SKTexture(imageNamed: "zombie")
        charNode.xScale = 1
        charNode.yScale = 1
        DispatchQueue.main.asyncAfter(deadline: .now() + hideTime * 3) { [unowned self] in
            self.hide()
        }
    }
    
    func hide(){
        if !isVisible{
            return
        }
        charNode.run(SKAction.moveBy(x: 0, y: -85, duration: 0.05))
        isVisible = false
    }
    
    func hitZombieAction(){
        isHit = true
        let delay = SKAction.wait(forDuration: 0.25)
        let hide = SKAction.moveBy(x: 0, y: -85, duration: 0.5)
        let notVisibleZombie = SKAction.run {
            [unowned self] in  self.isVisible = false
        }
        let sequenceAction = SKAction.sequence([delay,hide,notVisibleZombie])
        charNode.run(sequenceAction)
    }
    
}
