//
//  GameScene.swift
//  GameOn
//
//  Created by Anshima on 24/04/17.
//  Copyright © 2017 AnshimaSingh. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var scoreLabel : SKLabelNode?
    var scoreValue: Int = 0 {
        didSet {
            print("The value of score is: \(scoreValue)")
            scoreLabel?.text = "Score: \(scoreValue)"
        }
    }
    var holes = [WhackingClass]()
    var popUpTime = 0.9
    var numberofrounds = 0
    
    override func didMove(to view: SKView) {
        
        //Added the background to game
        let bg = SKSpriteNode(imageNamed: "background.jpg")
        let screenSize: CGRect = UIScreen.main.bounds
        bg.size = CGSize(width: screenSize.width, height: screenSize.height)
        bg.blendMode = .replace
        addChild(bg)
        
        //Added the score label
        scoreLabel = SKLabelNode(fontNamed: "Chalkduster")
        scoreLabel?.text = "Score: 0"
        scoreLabel?.fontColor = UIColor(red: 180/255, green: 10/255, blue: 30/255, alpha: 1)
        scoreLabel?.position = CGPoint(x: 160, y: 30)
        scoreLabel?.horizontalAlignmentMode = .right
        scoreLabel?.fontSize = 32
        addChild(scoreLabel!)
        
        //Adding slots on the screen
        for i in 0 ..< 5 {
            //print("in for loop");
            createSlot(pos: CGPoint(x: -300 + (i * 100), y: -120))
        }
        for i in 0 ..< 4 { createSlot(pos: CGPoint(x: -250 + (i * 100), y: -80)) }
        for i in 0 ..< 4 { createSlot(pos: CGPoint(x: -250 + (i * 100), y: -160)) }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [unowned self] in
            self.createZombie()
        }
    }
    
    func createZombie(){
        popUpTime *= 0.95
        numberofrounds += 1
        if numberofrounds >= 40{
            for hole in holes{
                hole.hide()
            }
            let gameOver = SKSpriteNode(imageNamed: "gameOver")
            gameOver.position = CGPoint(x: 0, y: 0)
            gameOver.zPosition = 1
            addChild(gameOver)
            return
        }
        
        holes = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: holes) as! [WhackingClass]
        holes[0].show(hideTime: popUpTime)
        if arc4random_uniform(12) > 4 { holes[1].show(hideTime: popUpTime) }
        if arc4random_uniform(12) > 7 { holes[2].show(hideTime: popUpTime) }
        if arc4random_uniform(12) > 10 { holes[3].show(hideTime: popUpTime) }
        
        let minGameDelay = popUpTime / 2.0
        let maxGameDelay = popUpTime * 2.0
        let delay = RandomDouble(min: minGameDelay, max: maxGameDelay)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) { [unowned self] in
            self.createZombie()
        }
    }
    
    func RandomDouble(min: Double, max: Double) -> Double {
        return (Double(arc4random()) / Double(UInt32.max)) * (max - min) + min
    }
    
    func createSlot(pos:CGPoint){
        
        //Creating a single hole object
        let hole = WhackingClass()
        hole.configureAtPosition(pos: pos)
        addChild(hole)
        
        //Adding hole to array holes
        holes.append(hole)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches{
            let loc = touch.location(in: self)
            let objs = nodes(at: loc) as [SKNode]
            for node in objs{
                if node.name == "zombieCharacter"{
                    let whackedObj = node.parent!.parent as! WhackingClass
                    //if !whackedObj.isVisible { continue }
                    //if !whackedObj.isHit {continue}
                   // print("whacked game")
                    whackedObj.hitZombieAction()
                    scoreValue = scoreValue + 1
                    whackedObj.charNode.xScale = 0.75
                    whackedObj.charNode.yScale = 0.75
                    
                    //Playing whack sound on hitting a zombie
                    run(SKAction.playSoundFileNamed("whack.caf", waitForCompletion: false))
                }
            }
        }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}
